# PostJS-specs

Just figuring out how we might Replace JavaScript with something "better". More declarative for webdevs, easier to optimize for browser engines, and easier to control for web surfers.

See the wiki for the draft specifications, and the specific goals of this project.

## Reference implementations
I (Adrian Cochrane) would recommend that two reference implementations to be created for them: 

### Fork Mozilla Servo
This would help show what challenges and opportunities would exist in implementing these specifications in a fairly real-world setting.

As for why I recommend Mozilla Servo, that's just because it is the most modular browser engine I've seen.

### JavaScript Polyfills
Having two very different implementations would help ensure these specifications aren't tied to a specific implementation. And the JavaScript polyfills would be vital in getting these specifications adopted as they would remove the need for the major browsers to implement them right off the bat.